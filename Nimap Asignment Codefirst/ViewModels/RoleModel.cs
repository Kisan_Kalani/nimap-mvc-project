﻿using Nimap_Asignment_Codefirst.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nimap_Asignment_Codefirst.ViewModels
{
    public class RoleModel
    {
        
        public string RoleId { get; set; }

        [Required]
        public string RoleName { get; set; }


        public ICollection<ApplicationUser> Users{ get; set; }

    }
}