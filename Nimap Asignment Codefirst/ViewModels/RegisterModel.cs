﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nimap_Asignment_Codefirst.ViewModels
{
    public class RegisterModel
    {
        

        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }


        [Required]
        [DataType(DataType.Password)]
        [StringLength(10,ErrorMessage ="Password Must be maximumam 10 letters")]
        public string Password { get; set; }



        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "Password and Confirm Password Don't Match.")]
        public string ConfirmPassword { get; set; }

       

        [Required]
        [DisplayName("Mobile Number")]
        [DataType(DataType.PhoneNumber)]
        [StringLength(10, ErrorMessage = "Only 10 Digits")]
        public string PhoneNumber { get; set; }

        [Required]
        public string RoleName { get; set; }





    }
}