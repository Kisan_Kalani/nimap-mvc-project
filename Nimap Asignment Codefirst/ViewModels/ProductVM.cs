﻿using Nimap_Asignment_Codefirst.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nimap_Asignment_Codefirst.ViewModels
{
    [Serializable]
    public class ProductVM
    {
        public int ProductId { get; set; }

       
        [DisplayName("Product Name")]
        public string ProductName { get; set; }

        
        [DisplayName("Product Price")]
        [Range(10,50000,ErrorMessage ="Price must be in between 10-50000")]
        public int Price { get; set; }

        
        [DisplayName("Upload Image")]
        public string ProdImg { get; set; }


       
        [DisplayName("Created By")]
        public string CreatedBy { get; set; }



        
        [DisplayName("Category Name")]
        public int CatId { get; set; }

        public SelectList CategoryList { get; set; }

        public IEnumerable<SelectListItem> Categoryserialize { get; set; }



        [DisplayName("Creator Name")]
        public string userId { get; set; }


        //public SelectList UserList { get; set; }

        public IEnumerable<SelectListItem> userSerialize { get; set; }


        [DisplayName("Current Image")]
        public string ExistingPhoto { set; get; }

      

        public HttpPostedFileBase ImageFile { set; get; }

        
        
    }
}