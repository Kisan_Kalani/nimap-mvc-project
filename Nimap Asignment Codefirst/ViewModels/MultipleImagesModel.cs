﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Nimap_Asignment_Codefirst.ViewModels
{
    public class MultipleImagesModel
    {

        [Required]
        [DisplayName("Multiple Images")]
        public IEnumerable<HttpPostedFileBase> multipleImgs{ get; set; }
    }
}