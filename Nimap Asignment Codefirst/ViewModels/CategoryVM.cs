﻿using Nimap_Asignment_Codefirst.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nimap_Asignment_Codefirst.ViewModels
{
    [Serializable]
   public class CategoryVM
    {

        public int CategoryId { get; set; }


        [Required]
        [DisplayName("Category Name")]
        public string CategoryName { get; set; }


        //[DisplayName("Created By")]
        //public string CreatedBy { get; set; }
        [Required]
        [DisplayName("Creator Name")]
        public string userId { get; set; }

        
        //public SelectList UserList { get; set; }

        public IEnumerable<SelectListItem> userSerialize { get; set; }


        public bool isSelected { get; set; }


    }
}