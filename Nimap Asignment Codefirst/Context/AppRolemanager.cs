﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Nimap_Asignment_Codefirst.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nimap_Asignment_Codefirst.Context
{
    public class AppRolemanager:RoleManager<IdentityRole>
    {

        public AppRolemanager(IRoleStore<IdentityRole,string> rolestore) : base(rolestore)
        {

        }

        public static AppRolemanager Create(IdentityFactoryOptions<AppRolemanager> options,IOwinContext context)
        {
            NimapDbContext db = new NimapDbContext();
        
            AppRolemanager role = new AppRolemanager(new RoleStore<IdentityRole>(db));

            return role;

        }

       
    }
}