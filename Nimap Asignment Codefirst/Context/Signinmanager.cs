﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Nimap_Asignment_Codefirst.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Nimap_Asignment_Codefirst.Context
{
    public class Signinmanager: SignInManager<ApplicationUser, string>
    {
        public Signinmanager(Usermanager userManager, IAuthenticationManager authenManager) : base(userManager,authenManager)
        {

        }


        public static Signinmanager Create(IdentityFactoryOptions<Signinmanager> options,IOwinContext context)
        {
            return new Signinmanager(context.GetUserManager<Usermanager>(), context.Authentication);
            
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return IdentityHelper.GenerateUserIdentityAsync(user, (Usermanager)UserManager);
        }

    }
}