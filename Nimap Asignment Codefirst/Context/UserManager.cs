﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Nimap_Asignment_Codefirst.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nimap_Asignment_Codefirst.Context
{
    public class Usermanager :UserManager<ApplicationUser>
    {
        public Usermanager(IUserStore<ApplicationUser> store): base(store)
        {

        }

        public static Usermanager Create(IdentityFactoryOptions<Usermanager> options,IOwinContext context)
        {
            NimapDbContext db = context.Get<NimapDbContext>();
            Usermanager userManager = new Usermanager(new UserStore<ApplicationUser>(db));

          
            userManager.UserValidator = new UserValidator<ApplicationUser>(userManager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail =true
                
            };

            userManager.PasswordValidator = new PasswordValidator() { 
                
                RequireLowercase=true,
                RequireUppercase=true
                
            };

            return userManager;
        }




        
    }
}