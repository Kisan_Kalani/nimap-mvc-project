﻿using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Nimap_Asignment_Codefirst.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Nimap_Asignment_Codefirst.Context
{
    public class NimapDbContext : IdentityDbContext<ApplicationUser>
    {
        public NimapDbContext() : base("NimapContext")
        {

        }

        public static NimapDbContext Create()
        {
            return new NimapDbContext();
        }

        public DbSet<Category> Categories { get; set; }

        public DbSet<Product> Products { get; set; }
        
        public DbSet<Customer> Customers { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}