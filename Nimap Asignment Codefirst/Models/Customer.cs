﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Nimap_Asignment_Codefirst.Models
{
    [Table("CustomerMaster")]
    [Serializable]
    public class Customer
    {

        [Key]
        public int CustId { get; set; }

       
        public string CustName { get; set; }

        
       
        public string Mobile { get; set; }


        public string Password { set; get; }

       
        public string Email { get; set; }

        
        


    }
}