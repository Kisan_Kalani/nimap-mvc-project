﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nimap_Asignment_Codefirst.Models
{
    [Table("ProductMaster")]
    public class Product
    {
        [Key]
        public int ProductId { get; set; }
        
        [DisplayName("Product Name")]
        public string ProductName { get; set; }
        
        
        public int Price { get; set; }

        
        [DisplayName("Product Image")]
        public string ProdImg { get; set; }



        //public string CreatedBy { get; set; }

        [ForeignKey("User")]
        [DisplayName("Created By")]
        public string UserId { get; set; }

        public ApplicationUser User { get; set; }



        [ForeignKey("Category")]
        public int CatId { set; get; } 


        public  Category Category { get; set; }
    }
}