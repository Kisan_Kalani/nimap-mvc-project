﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Nimap_Asignment_Codefirst.Models
{
    [Table("CategoryMaster")]
    public class Category
    {
        [Key]
        public  int  CategoryId  { get; set; }


        public string CategoryName { get; set; }

        //public string CreatedBy { get; set; }


        [ForeignKey("User")]
        [Display(Name ="Created By")]
        public string UserId { get; set; }

        public ApplicationUser User { get; set; }


        //public  ICollection<Product> Products{ get; set; }
    }
}