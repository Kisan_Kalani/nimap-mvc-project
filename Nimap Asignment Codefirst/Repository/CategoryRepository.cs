﻿using Nimap_Asignment_Codefirst.Context;
using Nimap_Asignment_Codefirst.Models;
using Nimap_Asignment_Codefirst.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Nimap_Asignment_Codefirst.Repository
{
    public class CategoryRepository : ICategory
    {
        private readonly NimapDbContext _dbContext;
        public CategoryRepository()
        {
            _dbContext = new NimapDbContext();
        }

        public int addCategory(Category category)
        {
            _dbContext.Categories.Add(category);
            return _dbContext.SaveChanges();
        }

        public IEnumerable<Category> categoryList()
        {
            List<Category>  catList = _dbContext.Categories.Include("User").AsNoTracking().ToList();

            return catList;
        }

        public int deleteCategory(Category category)
        {

            _dbContext.Entry(category).State = EntityState.Deleted;
            
            return _dbContext.SaveChanges();
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }

        public void updateCategory(Category category)
        {
            
            _dbContext.Entry(category).State = EntityState.Modified;
            
        }
    }
}