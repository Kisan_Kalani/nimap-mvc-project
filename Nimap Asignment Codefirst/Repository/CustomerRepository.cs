﻿using Nimap_Asignment_Codefirst.Context;
using Nimap_Asignment_Codefirst.Models;
using Nimap_Asignment_Codefirst.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nimap_Asignment_Codefirst.Repository
{
    public class CustomerRepository :ICustomer
    {
        private readonly NimapDbContext _dbContext;
        public CustomerRepository()
        {
            _dbContext = new NimapDbContext();
        }

        public int addCustomer(Customer customer)
        {
            _dbContext.Customers.Add(customer);
            return _dbContext.SaveChanges();
        }

        public Customer checkUser(string email, string password)
        {
           var  customer=  _dbContext.Customers.Where(e => e.Email.Equals(email,StringComparison.OrdinalIgnoreCase)
           && e.Password == password).FirstOrDefault();
           
            if (customer != null)
            {
                return customer;
            }

            return null;
        }

       
        public void Save()
        {
            _dbContext.SaveChanges();
        }


    }
}