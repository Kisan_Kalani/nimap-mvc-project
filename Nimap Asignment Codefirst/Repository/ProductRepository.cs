﻿ using Nimap_Asignment_Codefirst.Context;
using Nimap_Asignment_Codefirst.Models;
using Nimap_Asignment_Codefirst.Services;
using Nimap_Asignment_Codefirst.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;


namespace Nimap_Asignment_Codefirst.Repository
{
    public class ProductRepository : IProduct
    {
        private readonly NimapDbContext _dbContext;
        
        public ProductRepository()
        {
            _dbContext = new NimapDbContext();
        }


        public void addProduct(Product product)
        {
            _dbContext.Products.Add(product);
            _dbContext.SaveChanges();
        }


        public int deleteProduct(Product product)
        {
            _dbContext.Entry(product).State = EntityState.Deleted;
            return _dbContext.SaveChanges();
        }

        public IEnumerable<Product> productList()
        {
           List<Product> productList=_dbContext.Products.Include("Category").Include("User").ToList();

            return productList;
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }

        public void updateProduct(Product product)
        {
            _dbContext.Entry(product).State = EntityState.Modified;
        }
    }
}