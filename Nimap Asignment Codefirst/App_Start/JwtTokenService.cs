﻿using Microsoft.AspNet.Identity;
using Microsoft.IdentityModel.Tokens;
using Nimap_Asignment_Codefirst.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;

namespace Nimap_Asignment_Codefirst.App_Start
{
    public  class JwtTokenService
    {

        private const string Secret = "db3OIsj+BXE9NZDy0t8W3TcNekrF+2d/1sFnWG4HnV8TZY30iTOdtVWJG8abWvB1GlOgJuQZdcF2Luqm/hccMw==";
        private const string IssueR = "http://localhost:54784";
        private const string AudiencE = "http://localhost:54784";

        NimapDbContext db;
        public JwtTokenService()
        {
            db = new NimapDbContext();
        }

        public  string GenerateToken(string username)
        {
            int expireMinutes = 7200;
            string token = string.Empty;

            var user = db.Users.AsNoTracking().Where(x => (x.UserName == username || x.Email== username)).FirstOrDefault();
            
            if (user != null)
            {
                var symmetricKey = Convert.FromBase64String(Secret);
                var tokenHandler = new JwtSecurityTokenHandler();

                var now = DateTime.UtcNow;
                
                var userIdentity = new ClaimsIdentity();
                List<Claim> claims = new List<Claim>();

                claims.Add(new Claim(ClaimTypes.Name, user.UserName));

               
                foreach (var r in user.Roles) 
                {
                    
                    claims.Add(new Claim(ClaimTypes.Role, r.RoleId));
                }

                userIdentity.AddClaims(claims);

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Issuer = IssueR,
                    Audience = AudiencE,
                    Subject = userIdentity,
                    NotBefore =now,
                    Expires = now.AddMinutes(Convert.ToInt32(expireMinutes)),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(symmetricKey), SecurityAlgorithms.HmacSha256Signature)
                };

                var stoken = tokenHandler.CreateToken(tokenDescriptor);
                token = tokenHandler.WriteToken(stoken);
            }
            return token;
        }

        public ClaimsPrincipal GetPrincipal(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                if (jwtToken == null)
                    return null;

                var symmetricKey = Convert.FromBase64String(Secret);

                var validationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = new SymmetricSecurityKey(symmetricKey)
                };

                SecurityToken securityToken;
                var principal = tokenHandler.ValidateToken(token, validationParameters, out securityToken);

                return principal;
            }

            catch (Exception)
            {
                return null;
            }
        }


        public bool ValidateToken(string token, out string username)
        {
            username = null;

            var simplePrinciple = GetPrincipal(token);
            var identity = simplePrinciple.Identity as ClaimsIdentity;

            if (identity == null)
                return false;

            if (!identity.IsAuthenticated)
                return false;

            var usernameClaim = identity.FindFirst(ClaimTypes.Email);
            username = usernameClaim?.Value;

            if (string.IsNullOrEmpty(username))
                return false;

            // More validate to check whether username exists in system
            var name = identity.GetUserName();
            var user =db.Users.AsNoTracking().Where(u => u.UserName == name);
            if(user == null)
               return false;
            
            
            return true;
        }

        protected Task<IPrincipal> AuthenticateJwtToken(string token)
        {
            string username;

            if (ValidateToken(token, out username))
            {
                // based on username to get more information from database in order to build local identity

                var useR = db.Users.AsNoTracking().Where(u => u.UserName == username).SingleOrDefault();
               
                var claims = new List<Claim>();

                new Claim(ClaimTypes.Name, username);
                
                // Add more claims if needed: Roles, ...
                foreach (var r in useR.Roles)
                {
                    claims.Add(new Claim(ClaimTypes.Role, r.RoleId));
                }
                

                var identity = new ClaimsIdentity(claims, "Jwt");

                IPrincipal user = new ClaimsPrincipal(identity);

                return Task.FromResult(user);
            }

            return Task.FromResult<IPrincipal>(null);
        }
    }
}
