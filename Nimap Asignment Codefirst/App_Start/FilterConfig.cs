﻿using Nimap_Asignment_Codefirst.App_Start;
using System.Web;
using System.Web.Mvc;

namespace Nimap_Asignment_Codefirst
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new CustomAuthorizationAttribute());
            filters.Add(new HandleErrorAttribute());
            
        }
    }
}
