﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Nimap_Asignment_Codefirst.Context;
using Nimap_Asignment_Codefirst.Models;
using Owin;

[assembly: OwinStartup(typeof(Nimap_Asignment_Codefirst.Startup))]

namespace Nimap_Asignment_Codefirst
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            app.CreatePerOwinContext<NimapDbContext>(NimapDbContext.Create);
            app.CreatePerOwinContext<Usermanager>(Usermanager.Create);
            app.CreatePerOwinContext<Signinmanager>(Signinmanager.Create);
            app.CreatePerOwinContext<AppRolemanager>(AppRolemanager.Create);

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                LogoutPath= new PathString("/Account/Logout"),
                //Provider = new CookieAuthenticationProvider
                //{
                //    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<Usermanager, ApplicationUser>(
                //validateInterval: TimeSpan.FromMinutes(30),
                //regenerateIdentity: (manager, user) => IdentityHelper.GenerateUserIdentityAsync(user, manager))
                //}


            });

            

            // ConfigureOAuthTokenConsumption(app);
        }

        //private void ConfigureOAuthTokenConsumption(IAppBuilder app)
        //{
           // in web.config file under <appsetting> 
         // <add key = "Issuer" value="http://localhost:54784" />	
		//<add key = "Audience" value="http://localhost:54784" />
		//<add key = "SecretKey" value="abcdefghijklmnopqrstuvwxyz" />


        //    var issuer = ConfigurationManager.AppSettings["Issuer"];
        //    var audience = ConfigurationManager.AppSettings["Audience"];
        //    var secretkey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(ConfigurationManager.AppSettings["SecretKey"]));

        //    var credentials = new SigningCredentials(secretkey, SecurityAlgorithms.HmacSha256);

        //   //claims

        // var token = new JwtSecurityToken(
        //        issuer,
        //        audience,
        //        //claims,
        //        notBefore: DateTime.Now,
        //        expires: DateTime.UtcNow.AddDays(1),
        //        signingCredentials: credentials
        //        ); ;

        //    var result = new
        //    {
        //        token = new JwtSecurityTokenHandler().WriteToken(token)
        //    };
            
        //}
    }
}
