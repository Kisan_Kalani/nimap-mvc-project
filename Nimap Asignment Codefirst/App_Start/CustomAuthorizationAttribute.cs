﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Nimap_Asignment_Codefirst.App_Start
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class CustomAuthorizationAttribute : AuthorizeAttribute
    {

        //private readonly string role;

        public readonly string[] allowedroles;


        public CustomAuthorizationAttribute(params string[] roles)
        {
            this.allowedroles = roles;
        }



        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            bool isAuthorize = false;//base.AuthorizeCore(httpContext);


            foreach (var role in allowedroles)
            {
                if (role == "Customer")
                {
                    if (httpContext.Session["UserName"] != null)
                    {

                        //var customer = HttpContext.Current.Session["UserName"].ToString();

                        //if (customer != null)
                        //{
                        //user = Rolehelper.Customer;
                        //httpContext.Session.Remove("UserName");
                        isAuthorize = true;
                        //}
                    }
                }
                else if (role == "Supervisor")
                {
                    if (httpContext.Session["RoleID"] != null)
                    {
                        if (httpContext.Session["RoleID"].ToString() == Rolehelper.Supervisor)
                        {

                            //user = Rolehelper.Supervisor;
                            isAuthorize = true;
                           // httpContext.Session.Remove("RoleId");

                        }
                        else/* if (HttpContext.Current.Session["RoleID"].ToString() == Rolehelper.Admin)*/
                        {
                            //user = null;
                            //httpContext.Session.Remove("RoleId");
                            isAuthorize = false;
                        }
                    }

                }
                else
                {
                    isAuthorize = false;
                }

            }
            return isAuthorize;
        }
    }
}