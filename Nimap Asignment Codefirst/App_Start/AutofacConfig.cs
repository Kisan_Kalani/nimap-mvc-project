﻿using Autofac;
using Autofac.Integration.Mvc;
using Nimap_Asignment_Codefirst.Context;
using Nimap_Asignment_Codefirst.Models;
using Nimap_Asignment_Codefirst.Repository;
using Nimap_Asignment_Codefirst.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nimap_Asignment_Codefirst.App_Start
{
    public static class AutofacConfig
    {
       public static void RegisterComponents()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            builder.RegisterType<CategoryRepository>().As<ICategory>();
            builder.RegisterType<ProductRepository>().As<IProduct>();
            builder.RegisterType<CustomerRepository>().As<ICustomer>();

            

            IContainer container = builder.Build();

            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

        }



    }
}