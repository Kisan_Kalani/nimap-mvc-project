﻿using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Nimap_Asignment_Codefirst.Context;
using Nimap_Asignment_Codefirst.Models;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Nimap_Asignment_Codefirst
{
    public static class IdentityHelper
    {
        
        public static async Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUser user, UserManager<ApplicationUser> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);

           
            List<Claim> claims = new List<Claim>();



            claims.Add(new Claim(ClaimTypes.Email, user.Email));

            foreach(var r in user.Roles)
            {
                claims.Add(new Claim(ClaimTypes.Role,r.RoleId));
            }

            userIdentity.AddClaims(claims);

            return userIdentity; 
        }
    }
}