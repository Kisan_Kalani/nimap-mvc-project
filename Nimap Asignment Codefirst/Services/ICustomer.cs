﻿using Nimap_Asignment_Codefirst.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nimap_Asignment_Codefirst.Services
{
    public interface ICustomer
    {

        int addCustomer(Customer customer);

       

        Customer checkUser(string email, string Password);

        void Save();
    }
}