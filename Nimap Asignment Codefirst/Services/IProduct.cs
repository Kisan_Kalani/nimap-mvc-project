﻿using Nimap_Asignment_Codefirst.Models;
using Nimap_Asignment_Codefirst.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Nimap_Asignment_Codefirst.Services
{
    public interface IProduct
    {
        IEnumerable<Product> productList();

        void addProduct(Product product);


        void updateProduct(Product product);

        int deleteProduct(Product product);
        void Save();

    }
}
