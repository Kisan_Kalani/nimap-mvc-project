﻿using Nimap_Asignment_Codefirst.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nimap_Asignment_Codefirst.Services
{
    public interface ICategory
    {

       int addCategory(Category category);

        IEnumerable<Category> categoryList();

        int deleteCategory(Category category);

        void Save();

        void updateCategory(Category category);


    }
}
