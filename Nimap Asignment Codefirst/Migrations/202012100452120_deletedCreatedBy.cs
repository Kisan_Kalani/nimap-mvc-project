﻿namespace Nimap_Asignment_Codefirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class deletedCreatedBy : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.CategoryMaster", "CreatedBy");
            DropColumn("dbo.ProductMaster", "CreatedBy");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductMaster", "CreatedBy", c => c.String());
            AddColumn("dbo.CategoryMaster", "CreatedBy", c => c.String());
        }
    }
}
