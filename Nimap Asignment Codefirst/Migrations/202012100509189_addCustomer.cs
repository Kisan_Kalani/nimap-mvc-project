﻿namespace Nimap_Asignment_Codefirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCustomer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CategoryMaster", "UserId", c => c.String(maxLength: 128));
            AddColumn("dbo.ProductMaster", "UserId", c => c.String(maxLength: 128));
            CreateIndex("dbo.CategoryMaster", "UserId");
            CreateIndex("dbo.ProductMaster", "UserId");
            AddForeignKey("dbo.ProductMaster", "UserId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.CategoryMaster", "UserId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CategoryMaster", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ProductMaster", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.ProductMaster", new[] { "UserId" });
            DropIndex("dbo.CategoryMaster", new[] { "UserId" });
            DropColumn("dbo.ProductMaster", "UserId");
            DropColumn("dbo.CategoryMaster", "UserId");
        }
    }
}
