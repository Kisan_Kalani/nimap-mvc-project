﻿using Microsoft.AspNet.Identity.Owin;
using Nimap_Asignment_Codefirst.Context;
using Nimap_Asignment_Codefirst.Models;
using Nimap_Asignment_Codefirst.Services;
using Nimap_Asignment_Codefirst.ViewModels;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nimap_Asignment_Codefirst.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CategoryController : Controller
    {
        private readonly ICategory _iCategory;
        private Usermanager _usermanager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<Usermanager>();
            }
        }


        public CategoryController(ICategory iCategory)
        {
            _iCategory = iCategory;
        }


        public ActionResult CategoryList(string search, int? page, int? pageSize)
        {
            int defaultPageSize = pageSize ?? 3;
            ViewBag.psize = defaultPageSize;

            ViewBag.PageSize = new List<SelectListItem>()
             {
             new SelectListItem() { Value="3", Text= "3" },
             new SelectListItem() { Value="10", Text= "10" },
             new SelectListItem() { Value="15", Text= "15" }

             };

            if (search != null)
            {
                var data = _iCategory.categoryList().Where(s => s.CategoryName.StartsWith(search, StringComparison.OrdinalIgnoreCase) || search == null)
                    .ToPagedList(page ?? 1, defaultPageSize);
                return View(data);
            }
            else
            {

                var data = _iCategory.categoryList().ToPagedList(page ?? 1, defaultPageSize);
                return View(data);
            }

        }


        public ActionResult CategoryDetails(int id)
        {

            Category category = _iCategory.categoryList().FirstOrDefault(x => x.CategoryId == id);
            return View(category);
        }

        [HttpGet]
        // GET: Category/Create
        public ActionResult CategoryCreate()
        {
            CategoryVM model = new CategoryVM();
            //IEnumerable<ApplicationUser> users = _usermanager.Users;
            //SelectList userist = new SelectList(users, "Id", "UserName");
            //model.UserList = userist;

            var ienumCuserList = _usermanager.Users as IEnumerable<ApplicationUser>;
            List<SelectListItem> userSe = new List<SelectListItem>();
            foreach (var item in ienumCuserList)
            {
                userSe.Add(new SelectListItem() { Text = item.UserName, Value = item.Id.ToString() });
            }
            model.userSerialize = userSe;

            

            return View(model);
        }

        // POST: Category/Create
        [HttpPost]
        public ActionResult CategoryCreate(CategoryVM model)
        {

            if (ModelState.IsValid)
            {
                Category cat = new Category()
                {
                    CategoryName = model.CategoryName,
                    UserId = model.userId
                    //CreatedBy = model.CreatedBy
                    

                };

                int id = _iCategory.addCategory(cat);

                return RedirectToAction("CategoryList");

            }
            else
            {
                return View();
            }

        }

        [HttpGet]
        public ActionResult CategoryEdit(int id)
        {
            Category cat = _iCategory.categoryList().FirstOrDefault(x => x.CategoryId == id);
            CategoryVM model = new CategoryVM()
            {
                CategoryId = cat.CategoryId,
                CategoryName = cat.CategoryName,
                userId=cat.UserId
                //CreatedBy = cat.CreatedBy
            };

            //IEnumerable<ApplicationUser> users = _usermanager.Users;
            //model.UserList = new SelectList(users, "Id", "UserName");

            var ienumCuserList = _usermanager.Users as IEnumerable<ApplicationUser>;
            List<SelectListItem> userSe = new List<SelectListItem>();
            foreach (var item in ienumCuserList)
            {
                userSe.Add(new SelectListItem() { Text = item.UserName, Value = item.Id.ToString() });
            }
            model.userSerialize = userSe;


            return View(model);
        }

        // POST: Category/Edit/5
        [HttpPost]
        public ActionResult CategoryEdit(CategoryVM model)
        {

            if (ModelState.IsValid)
            {

                Category cat = new Category() { 
                CategoryId= model.CategoryId,
                CategoryName=model.CategoryName,
                UserId = model.userId
                };

                

                _iCategory.updateCategory(cat);
                _iCategory.Save();

                return RedirectToAction("CategoryList");

            }
            else
            {
                return View(model);
            }

        }


        [HttpPost]
        public ActionResult CategoryDelete(int id)
        {
            Category category = _iCategory.categoryList().FirstOrDefault(c => c.CategoryId == id);
           int catid = _iCategory.deleteCategory(category);

            return RedirectToAction("CategoryList");

        }
    }
}
