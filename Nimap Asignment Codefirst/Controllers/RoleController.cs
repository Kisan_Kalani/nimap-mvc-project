﻿
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Nimap_Asignment_Codefirst.Context;
using Nimap_Asignment_Codefirst.Models;
using Nimap_Asignment_Codefirst.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Nimap_Asignment_Codefirst.Controllers
{
    [Authorize(Roles ="Admin")]
    public class RoleController : Controller
    {

        private Usermanager _usermanager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<Usermanager>();
            }
        }

        private AppRolemanager _rolemanager
        {
            get
            {
                return HttpContext.GetOwinContext().Get<AppRolemanager>();
            }
        }

        public RoleController()
        {

        }


        #region RoleCRUD
        public async Task<ActionResult> RoleList()
        {
            var rolesList = await _rolemanager.Roles.ToListAsync();
            return View(rolesList);
        }


        [HttpGet]
        public ActionResult RoleCreate()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> RoleCreate(RoleModel model)
        {
            if (ModelState.IsValid)
            {
                IdentityRole role = new IdentityRole()
                {
                    Name = model.RoleName
                };

                var result = await _rolemanager.CreateAsync(role);

                if (result.Succeeded)
                {
                    return RedirectToAction("RoleList");
                }


            }

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> RoleEdit(string id)
        {
            var role = await _rolemanager.FindByIdAsync(id);
            RoleModel model = new RoleModel()
            {
                RoleId = role.Id,
                RoleName = role.Name

            };

            return View(model);
        }


        [HttpPost]
        public async Task<ActionResult> RoleEdit(RoleModel model)
        {

            IdentityRole role = new IdentityRole()
            {
                Id = model.RoleId,
                Name = model.RoleName
            };
            var result = await _rolemanager.UpdateAsync(role);

            if (result.Succeeded)
            {
                return RedirectToAction("RoleList");
            }

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> RoleDelete(string id)
        {
            var role = await _rolemanager.FindByIdAsync(id);
            var result = await _rolemanager.DeleteAsync(role);

            if (result.Succeeded)
            {
                return RedirectToAction("RoleList");
            }
            return View();
        }

        #endregion

        #region Users to Role 



        [HttpGet]
        public async Task<ActionResult> AddRoleToUsers(string id)
        {

            ViewBag.RoleId = id;

            var role = await _rolemanager.FindByIdAsync(id);
            ViewBag.RoleName = role.Name;
            var model = new List<UserRoleModel>();


            foreach (var user in _usermanager.Users.ToList())
            {
                UserRoleModel userRoleModel = new UserRoleModel()
                {
                    UserId = user.Id,
                    UserName = user.UserName,
                };


                if (await _usermanager.IsInRoleAsync(user.Id, role.Name))
                {

                    userRoleModel.IsSelected = true;
                }
                else
                {
                    userRoleModel.IsSelected = false;
                }



                model.Add(userRoleModel);
            }

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> AddRoleToUsers(List<UserRoleModel> model, string roleid)
        {
            if (model != null)
            {
                var role = await _rolemanager.FindByIdAsync(roleid);

                for (int i = 0; i < model.Count; i++)
                {

                    var user = await _usermanager.FindByIdAsync(model[i].UserId);

                    IdentityResult result = null;

                    if (model[i].IsSelected && (!await _usermanager.IsInRoleAsync(user.Id, role.Name)))
                    {
                        result = await _usermanager.AddToRoleAsync(user.Id, role.Name);

                    }
                    else if ((!model[i].IsSelected) && (await _usermanager.IsInRoleAsync(user.Id, role.Name)))
                    {

                        result = await _usermanager.RemoveFromRoleAsync(user.Id, role.Name);
                    }
                    else
                    {
                        continue;
                    }

                    if (result.Succeeded)
                    {
                        if (i < (model.Count - 1))
                        {
                            continue;
                        }
                        else
                        {
                            return RedirectToAction("Rolelist");
                        }
                    }
                }

            }
            return RedirectToAction("Rolelist");

        }
        #endregion



    }
}
