﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using Nimap_Asignment_Codefirst.Models;
using Nimap_Asignment_Codefirst.Services;
using Nimap_Asignment_Codefirst.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Nimap_Asignment_Codefirst.Controllers
{
    public class ShoppingCartController : Controller
    {
        private readonly IProduct _product;
        public ShoppingCartController(IProduct product)
        {
            _product = product;
        }



        [HttpPost]
        public ActionResult AddToCart(int id)
        {
            OrderModel model = new OrderModel();
            Product prod = _product.productList().FirstOrDefault(x => x.ProductId == id);
            if (Session["cart"] == null)
            {

                model.li = new List<Product>();
                model.li.Add(prod);
                try
                {
                    string JsonList = JsonConvert.SerializeObject(model.li, new JsonSerializerSettings()
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                        Formatting = Formatting.Indented
                    });

                    Session["cart"] = JsonList;//model.li
                    ViewBag.cart = model.li.Count();

                    Session["count"] = 1;
                }
                catch (JsonSerializationException e)
                {
                     
                }
               

            }
            else
            {
                var jsonDeserializeCatList = JsonConvert.DeserializeObject<List<Product>>(Session["cart"].ToString());

                model.li = jsonDeserializeCatList;//(List<Product>)Session["cart"];
                model.li.Add(prod);

                string JsonList = JsonConvert.SerializeObject(model.li, new JsonSerializerSettings()
                {
                    PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                    Formatting = Formatting.Indented
                });

                Session["cart"] = JsonList;
                ViewBag.cart = model.li.Count();
                Session["count"] = Convert.ToInt32(Session["count"]) + 1;

            }
            return RedirectToAction("ProductList", "Product");

        }


        public ActionResult Myorder()
        {
            OrderModel model = new OrderModel();

            var jsonDeserializeCatList = JsonConvert.DeserializeObject<List<Product>>(Session["cart"].ToString());
            model.li = jsonDeserializeCatList;//(List<Product>)Session["cart"];
            return View(model);//View((List<Product>)Session["cart"]);

        }



        [HttpPost]
        public ActionResult RemoveFromCart(int id)
        {

            Product prod = _product.productList().FirstOrDefault(p => p.ProductId == id);
            var jsonDeserializeCatList = JsonConvert.DeserializeObject<List<Product>>(Session["cart"].ToString());
            List<Product> li = jsonDeserializeCatList;//(List<Product>)Session["cart"];
            
            li.RemoveAll(x => x.ProductId == prod.ProductId);


            string JsonList = JsonConvert.SerializeObject(li, new JsonSerializerSettings()
            {
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                Formatting = Formatting.Indented
            });


            Session["cart"] = JsonList;//li
            Session["count"] = Convert.ToInt32(Session["count"]) - 1;

            int cou = Convert.ToInt32(Session["count"]);
            if (cou > 0)
            {
                return RedirectToAction("Myorder", "ShoppingCart");
            }
            return RedirectToAction("ProductList", "Product");
        }

        #region CreatePDF


        public FileResult CreatePdf(int sum)
        {
            MemoryStream workStream = new MemoryStream();
            StringBuilder status = new StringBuilder("");
            DateTime dTime = DateTime.Now;
            // file name to be created 
            string strPDFFileName = string.Format("MyOrderPdf" + dTime.ToString("yyyyMMdd") + "-" + ".pdf");
            Document doc = new Document();
            doc.SetMargins(10f, 10f, 10f, 10f);
            // Create PDF Table with 5 coulmns
            PdfPTable tableLayout = new PdfPTable(3);
            //doc.SetMargins(0f, 0f, 0f, 0f);

            // Create PDF Table

            // file will created in this path
            string strAttachment = Server.MapPath("~/Downloads" + strPDFFileName);

            PdfWriter.GetInstance(doc, workStream).CloseStream = false;
            doc.Open();

            //Add content to PDF
            doc.Add(Add_Content_To_PDF(tableLayout, sum));

            // closing the document
            doc.Close();

            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;

            return File(workStream, "application/pdf", strPDFFileName);
        }

        private PdfPTable Add_Content_To_PDF(PdfPTable tableLayout, int sum)
        {
            //float[] headers = { 50, 24, 45, 35, 50 };   // header widths
            float[] headers = { 50, 45, 24 };   // header widths
            tableLayout.SetWidths(headers);             // Set the pdf headers
            tableLayout.WidthPercentage = 100;
            tableLayout.HeaderRows = 1;

            // Add Title to the PDF file at the Top

            //List<Product> product = db.Products.ToList<Product>();
            //var invoice = (List<Product>)Session["cart"];
            OrderModel model = new OrderModel();

            var jsonDeserializeCatList = JsonConvert.DeserializeObject<List<Product>>(Session["cart"].ToString());

            model.li = jsonDeserializeCatList;//(List<Product>)Session["cart"];
            model.total = sum;
            var invoice = model;


            tableLayout.AddCell(new PdfPCell(new Phrase("InVoice Of Placed Orders", new Font(Font.FontFamily.HELVETICA, 8, 1,
                new BaseColor(0, 0, 0))))
            { Colspan = 12, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });


            tableLayout.AddCell(new PdfPCell(new Phrase("Customer Name  :   " + Session["Username"].ToString(), new Font(Font.FontFamily.HELVETICA, 8, 1,
               new BaseColor(0, 0, 0))))
            { Colspan = 12, Border = 2, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });

            tableLayout.AddCell(new PdfPCell(new Phrase("Contact Number :   " + Session["UserMobile"].ToString(), new Font(Font.FontFamily.HELVETICA, 8, 1,
              new BaseColor(0, 0, 0))))
            { Colspan = 12, Border = 2, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_LEFT });
            // Add Header

            AddCellToHeader(tableLayout, "Product Id");
            AddCellToHeader(tableLayout, "Product Name");
            AddCellToHeader(tableLayout, "Product Price");

            // Add Body
            if (invoice != null)
            {
                foreach (var pro in invoice.li)
                {
                    AddCellToBody(tableLayout, pro.ProductId.ToString());
                    AddCellToBody(tableLayout, pro.ProductName);
                    AddCellToBody(tableLayout, pro.Price.ToString());
                }

            }


            tableLayout.AddCell(new PdfPCell(new Phrase("Total Amount                   Rs. " + invoice.total.ToString() + "/-", new Font(Font.FontFamily.HELVETICA, 8, 1,
                new BaseColor(0, 0, 0))))
            { Colspan = 3, Border = 2, PaddingTop = 5, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });


            tableLayout.AddCell(new PdfPCell(new Phrase("*****************THANK YOU FOR SHOPPING*****************", new Font(Font.FontFamily.HELVETICA, 8, 1,
                 new BaseColor(0, 0, 0))))
            { Colspan = 12, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });


            // AddCellToBody(tableLayout, );
            return tableLayout;
        }

        // Method to add single cell to the Header
        private static void AddCellToHeader(PdfPTable tableLayout, string cellText)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, BaseColor.YELLOW))) { HorizontalAlignment = Element.ALIGN_LEFT, Padding = 5, BackgroundColor = new BaseColor(128, 0, 0) });
        }

        // Method to add single cell to the body
        private static void AddCellToBody(PdfPTable tableLayout, string cellText)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, BaseColor.BLACK))) { HorizontalAlignment = Element.ALIGN_LEFT, Padding = 5, BackgroundColor = new BaseColor(255, 255, 255) });
        }
        #endregion

    }
}