﻿using Microsoft.AspNet.Identity.Owin;
using Nimap_Asignment_Codefirst.App_Start;
using Nimap_Asignment_Codefirst.Context;
using Nimap_Asignment_Codefirst.Models;
using Nimap_Asignment_Codefirst.Services;
using Nimap_Asignment_Codefirst.ViewModels;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nimap_Asignment_Codefirst.Controllers
{

    //[Authorize(Roles = "Supervisor")]
    

    //[CustomAuthorization(sessionuser: Rolehelper.roleid)]
    public class ProductController : Controller
    {
        private readonly IProduct _product;
        private readonly ICategory _category;


        private Usermanager _usermanager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<Usermanager>();
            }
        }



        public ProductController(IProduct product, ICategory category)
        {
            _product = product;
            _category = category;

        }


        #region ProductCRUD

        [CustomAuthorization("Supervisor", "Customer")]
        public ActionResult ProductList(string search, int? page, int? pageSize, int? minPrice, int? maxPrice, List<CategoryVM> cateDataQuery)
        {
            int defaultPageSize = pageSize ?? 3;
            ViewBag.psize = defaultPageSize;

            var categoryList = new List<CategoryVM>();

            
            if (cateDataQuery != null)
            {
                
                if (cateDataQuery.Count > 0)
                {
                    categoryList = cateDataQuery.Where(x => x.isSelected == true).ToList();

                    Session["CatFilter"] = categoryList;

                }
            }
            else
            {
                Session.Remove("CatFilter");
            }


            categoryList = Session["CatFilter"] as List<CategoryVM>;
            
            var CatList = _category.categoryList().ToList();

            List<CategoryVM> modelList = new List<CategoryVM>();
            foreach (var cat in CatList)
            {
                CategoryVM model = new CategoryVM();
                model.CategoryId = cat.CategoryId;
                model.CategoryName = cat.CategoryName;

                modelList.Add(model);
            }

            var viewDataDict = new ViewDataDictionary(modelList);

            ViewData["viewDataDict"] = viewDataDict;




            ViewBag.PageSize = new List<SelectListItem>(){
            new SelectListItem() {Text= "3",Value = "3" },
            new SelectListItem() { Text = "10", Value = "10" },
            new SelectListItem() { Text = "15", Value = "15" }
            };
            //IEnumerable<SelectListItem> DDpage = pageSizea;

            //ViewBag.PageSize = DDpage;

            if (minPrice != null && maxPrice != null && categoryList.Count>0)
            {

                List<Product> FinalList = new List<Product>();
                for (int i = 0; i < categoryList.Count; i++)
                {
                    var EnumProd = _product.productList()
                            .Where(c => c.Category.CategoryName.Equals(categoryList[i].CategoryName, StringComparison.OrdinalIgnoreCase) 
                            &&
                            (c.Price>=minPrice && c.Price<=maxPrice));
                         
                    FinalList.AddRange(EnumProd);
                   
                }
                var data = FinalList.OrderBy(p=>p.Price).ToPagedList(page ?? 1, defaultPageSize);
                return View(data);
            }


            if (minPrice != null && maxPrice != null)
            {
                var data = _product.productList().Where(p => p.Price >= minPrice && p.Price <= maxPrice).OrderBy(x => x.Price).ToPagedList(page ?? 1, defaultPageSize);
                return View(data);
            }
            

            if (categoryList != null)
            {
                //IPagedList<Product> data = null;
                List<Product> FinalList = new List<Product>();

                for (int i = 0; i < categoryList.Count; i++)
                {
                    var EnumProd = _product.productList().Where(c => c.Category.CategoryName.Equals(categoryList[i].CategoryName, StringComparison.OrdinalIgnoreCase));

                    FinalList.AddRange(EnumProd);
                    //data = _product.productList()
                    //                 .Where(c => c.Category.CategoryName.Equals(categoryList[i].CategoryName, StringComparison.OrdinalIgnoreCase))
                    //                 .ToPagedList(page ?? 1, defaultPageSize);

                }
                var data = FinalList.ToPagedList(page ?? 1, defaultPageSize);
              
                return View(data);
            }

            if (search != null)
            {
                var data = _product.productList().Where(s => s.ProductName.StartsWith(search, StringComparison.OrdinalIgnoreCase) || search == null)
                    .ToPagedList(page ?? 1, defaultPageSize);
                return View(data);
            }
            else
            {

                var data = _product.productList().ToPagedList(page ?? 1, defaultPageSize);

                return View(data);
            }
        }

       

        [AllowAnonymous]
        [HttpGet]
        public ActionResult ProductDetails(int id)
        {

            Product product = _product.productList().FirstOrDefault(x => x.ProductId == id);
            return View(product);
        }

        [CustomAuthorization("Supervisor")]
        public ActionResult ProductCreate()
        {
            ProductVM model = new ProductVM();
            //SelectList catList = new SelectList(_category.categoryList(), "CategoryId", "CategoryName");
            //model.CategoryList = catList;
            //model.UserList = Userlist();

            var ienumCuserList = _usermanager.Users as IEnumerable<ApplicationUser>;
            List<SelectListItem> userSe = new List<SelectListItem>();
            foreach (var item in ienumCuserList)
            {
                userSe.Add(new SelectListItem() { Text = item.UserName, Value = item.Id.ToString() });
            }
            model.userSerialize = userSe;




            var ienumCatList = _category.categoryList();

            List<SelectListItem> catSe= new List<SelectListItem>();
            foreach(var item in ienumCatList)
            {
                catSe.Add(new SelectListItem() { Text = item.CategoryName, Value = item.CategoryId.ToString() });
            }

            model.Categoryserialize = catSe;


            return View(model);
        }


        [CustomAuthorization("Supervisor")]
        [HttpPost]
        public ActionResult ProductCreate(ProductVM model)
        {

            if (ModelState.IsValid)
            {

                Product product = new Product()
                {
                    ProductName = model.ProductName,
                    Price = model.Price,
                    CatId = model.CatId,
                    UserId = model.userId
                    //CreatedBy = model.CreatedBy,

                };

                product.ProdImg = ProcessImage(model);

                _product.addProduct(product);
                ModelState.Clear();

                return RedirectToAction("ProductList");
            }
            else
            {

                return View();
            }
        }


        [CustomAuthorization("Supervisor")]
        [HttpGet]
        public ActionResult ProductEdit(int id)
        {
            Product product = _product.productList().FirstOrDefault(p => p.ProductId == id);

            ProductVM model = new ProductVM()
            {
                ProductId = product.ProductId,
                ProductName = product.ProductName,
                CatId = product.CatId,
                userId = product.UserId,
                //CreatedBy = product.CreatedBy,
                ExistingPhoto = product.ProdImg,
                Price = product.Price
            };


            //model.CategoryList = new SelectList(_category.categoryList(), "CategoryId", "CategoryName");

            var ienumCatList = _category.categoryList();
            List<SelectListItem> catSe = new List<SelectListItem>();
            foreach (var item in ienumCatList)
            {
                catSe.Add(new SelectListItem() { Text = item.CategoryName, Value = item.CategoryId.ToString() });
            }
            model.Categoryserialize = catSe;
            
            
            //model.UserList = Userlist();
            var ienumCuserList = _usermanager.Users as IEnumerable<ApplicationUser>;
            List<SelectListItem> userSe = new List<SelectListItem>();
            foreach (var item in ienumCuserList)
            {
                userSe.Add(new SelectListItem() { Text = item.UserName, Value = item.Id.ToString() });
            }
            model.userSerialize = userSe;
            return View(model);

        }

        [CustomAuthorization("Supervisor")]
        [HttpPost]
        public ActionResult ProductEdit(ProductVM model)
        {

            if (ModelState.IsValid)
            {

                Product product = new Product()
                {
                    ProductId = model.ProductId,
                    ProductName = model.ProductName,
                    Price = model.Price,
                    CatId = model.CatId,
                    UserId = model.userId
                    //CreatedBy = model.CreatedBy,

                };


                if (model.ImageFile != null)
                {
                    if (model.ExistingPhoto != null)
                    {
                        string[] fileNameSplit = model.ExistingPhoto.Split('/');
                        string fName = fileNameSplit[fileNameSplit.Length - 1];

                        string filename = Path.Combine(Server.MapPath("~/Image/"), fName);
                        System.IO.File.Delete(filename);
                    }

                    product.ProdImg = ProcessImage(model);
                }

                _product.updateProduct(product);
                _product.Save();


                return RedirectToAction("ProductList");
            }
            else
            {

                return View();
            }
        }


        [CustomAuthorization("Supervisor")]
        [HttpPost]
        public ActionResult ProductDelete(int id)
        {

            Product product = _product.productList().FirstOrDefault(p => p.ProductId == id);

            int pid = _product.deleteProduct(product);

            return RedirectToAction("ProductList");

        }
        #endregion


        #region Import Excel

        [CustomAuthorization("Supervisor")]
        [HttpGet]
        public ActionResult ImportFromExcel()
        {
            return View();
        }

        [CustomAuthorization("Supervisor")]
        [HttpPost]
        public ActionResult ImportFromExcel(HttpPostedFileBase postedFile)
        {
            if (ModelState.IsValid)
            {
                if (postedFile != null && postedFile.ContentLength > (1024 * 1024 * 50))  // 50MB limit  
                {
                    ModelState.AddModelError("postedFile", "Your file is to large. Maximum size allowed is 50MB !");
                }

                else
                {
                    string filePath = string.Empty;

                    string path = Server.MapPath("~/Uploads/");
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    filePath = path + Path.GetFileName(postedFile.FileName);
                    string extension = Path.GetExtension(postedFile.FileName);
                    postedFile.SaveAs(filePath);

                    string conString = string.Empty;

                    switch (extension)
                    {
                        case ".xls": //For Excel 97-03.  
                            conString = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                            break;
                        case ".xlsx": //For Excel 07 and above.  
                            conString = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                            break;
                    }

                    try
                    {
                        DataTable dt = new DataTable();
                        conString = string.Format(conString, filePath);

                        using (OleDbConnection connExcel = new OleDbConnection(conString))
                        {
                            using (OleDbCommand cmdExcel = new OleDbCommand())
                            {
                                using (OleDbDataAdapter odaExcel = new OleDbDataAdapter())
                                {
                                    cmdExcel.Connection = connExcel;

                                    //Get the name of First Sheet.  
                                    connExcel.Open();
                                    DataTable dtExcelSchema;
                                    dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                    string sheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
                                    
                                    connExcel.Close();

                                    //Read Data from First Sheet.  
                                    connExcel.Open();
                                    
                                    cmdExcel.CommandText = "SELECT * From [" + sheetName + "]";
                                    odaExcel.SelectCommand = cmdExcel;
                                    odaExcel.Fill(dt);
                                    dt.TableName = sheetName.Replace("$", "");
                                    connExcel.Close();
                                }
                            }
                        }

                        conString = ConfigurationManager.ConnectionStrings["NimapContext"].ConnectionString;
                        using (SqlConnection con = new SqlConnection(conString))
                        {
                            using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                            {

                                
                                //Set the database table name.
                                sqlBulkCopy.DestinationTableName = "ProductMaster";
                                sqlBulkCopy.ColumnMappings.Add("ProductName", "ProductName");
                                sqlBulkCopy.ColumnMappings.Add("Price", "Price");
                                sqlBulkCopy.ColumnMappings.Add("ProdImg", "ProdImg");
                                sqlBulkCopy.ColumnMappings.Add("CatId", "CatId");
                                sqlBulkCopy.ColumnMappings.Add("UserId", "UserId");
                               
                                con.Open();
                               
                                sqlBulkCopy.WriteToServer(dt);
                                    
                                con.Close();

                                ViewBag.Result = "Successfully Imported";
                                return RedirectToAction("ImportFromExcel");
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        ViewBag.error = e.Message + "\n" + e.StackTrace;
                        return View("ImportFromExcel");
                    }

                }


            }

            return View(postedFile);
        }

        #endregion


        #region Multiple Image Uppload
        [CustomAuthorization("Supervisor")]
        [HttpGet]
        public ActionResult MultipleImages()
        {

            return View();
        }

        [CustomAuthorization("Supervisor")]
        [HttpPost]
        public ActionResult MultipleImages(MultipleImagesModel model)
        {
            if (ModelState.IsValid)
            {
                foreach (HttpPostedFileBase image in model.multipleImgs)
                {

                    if (image != null && image.ContentLength <= 10485760)//less than  10MB each
                    {
                        string filename = Path.GetFileNameWithoutExtension(image.FileName);

                        string extension = Path.GetExtension(image.FileName);
                        filename = filename + DateTime.Now.ToString("yymmssfff") + extension;

                        filename = Path.Combine(Server.MapPath("~/Image/"), filename);
                        if (extension == ".png" || extension == ".jpg" || extension == ".jpeg")
                        {
                            image.SaveAs(filename);

                        }
                        else
                        {
                            return View();
                        }
                    }

                }

                return RedirectToAction("ProductList");
            }

            return View();

        }
        #endregion

        [NonAction]
        private string ProcessImage(ProductVM model)
        {

            if (model.ImageFile != null)
            {
                string filename = Path.GetFileNameWithoutExtension(model.ImageFile.FileName);

                string extension = Path.GetExtension(model.ImageFile.FileName);
                filename = filename + DateTime.Now.ToString("yymmssfff") + extension;
                model.ProdImg = "~/Image/" + filename;


                filename = Path.Combine(Server.MapPath("~/Image/"), filename);

                model.ImageFile.SaveAs(filename);


            }


            return model.ProdImg;

        }

       
        //private SelectList Userlist()
        //{

        //    IEnumerable<ApplicationUser> users = _usermanager.Users;

        //    SelectList userList = new SelectList(users, "Id", "UserName");
        //    return userList;
        //}





    }
}
