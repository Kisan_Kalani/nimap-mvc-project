﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Nimap_Asignment_Codefirst.App_Start;
using Nimap_Asignment_Codefirst.Context;
using Nimap_Asignment_Codefirst.Models;
using Nimap_Asignment_Codefirst.Services;
using Nimap_Asignment_Codefirst.ViewModels;
using System;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ServiceStack.Redis;
using System.Configuration;

namespace Nimap_Asignment_Codefirst.Controllers
{
    public class AccountController : Controller
    {
        private readonly ICustomer _customer;

        public AccountController(ICustomer customer)
        {
            _customer = customer;
        }

        private Usermanager _usermanager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<Usermanager>();
            }
        }


        private Signinmanager _signinmanager
        {
            get
            {
                return HttpContext.GetOwinContext().Get<Signinmanager>();
            }
        }

        private AppRolemanager _rolemanager
        {
            get
            {
                return HttpContext.GetOwinContext().Get<AppRolemanager>();
            }
        }


        [HttpGet]
        public ActionResult Register()
        {

            return View();
        }


        [HttpPost]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                var userExist = await _usermanager.FindByEmailAsync(model.Email);
                if (userExist != null)
                {
                    ModelState.AddModelError("", "User Already Exits");
                }
                else
                {

                    if (model.RoleName == "Admin" || model.RoleName == "Supervisor")
                    {
                        var user = new ApplicationUser()
                        {
                            UserName = model.Email,
                            Email = model.Email,
                            PhoneNumber = model.PhoneNumber
                        };
                        var result = await _usermanager.CreateAsync(user, model.Password);

                      

                        if (result.Succeeded)
                        {
                                 await  _usermanager.AddToRoleAsync(user.Id,model.RoleName);
                           
                            await _signinmanager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                            return RedirectToAction("Login", "Account");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Registeration Failed");
                        }
                    }
                    else if (model.RoleName == "Customer")
                    {
                        var customer = new Customer()
                        {
                            CustName = model.Email,
                            Email = model.Email,
                            Mobile = model.PhoneNumber,
                            Password = model.Password
                        };

                        int custId = _customer.addCustomer(customer);

                        if (custId > 0)
                        {
                            return RedirectToAction("Login", "Account");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Registeration Failed");
                        }

                    }

                }
            }

            return View(model);
        }


        [HttpGet]
        public ActionResult Login(string ReturnUrl)
        {
            HttpCookie cookie = Request.Cookies["Customer"];
            if (cookie != null)
            {
                ViewBag.username = cookie["UserName"].ToString();

                //Decrypt Password from Cookie
                string encryptedPassword = cookie["Password"].ToString();
                byte[] b = Convert.FromBase64String(encryptedPassword);
                string decryptedPassword = ASCIIEncoding.ASCII.GetString(b);

                ViewBag.password = decryptedPassword;

            }

            ViewBag.ReturnUrl = ReturnUrl;

            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginModel model, string ReturnUrl)
        {
            if (ModelState.IsValid)
            {

                var user = await _usermanager.FindByEmailAsync(model.Email);

                if (user != null)
                {
                    var result = await _signinmanager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
                    JwtTokenService jwttoken = new JwtTokenService();
                    string token = jwttoken.GenerateToken(model.Email);

                    switch (result)
                    {
                        case SignInStatus.Success:
                            //kalanik12 =admin =category(Add and Edit)
                            //kalanik002=supervisor= Product(Add and EDit)
                            var claimsPrincipal = jwttoken.GetPrincipal(token);

                            foreach (var r in user.Roles)
                            {
                                //var role = _rolemanager.Roles.Where(x => x.Id == r.RoleId).FirstOrDefault();
                                if (claimsPrincipal.HasClaim(x => x.Type == ClaimTypes.Role && x.Value == r.RoleId) &&
                                    claimsPrincipal.HasClaim(x => x.Type == ClaimTypes.Name && x.Value == user.Email))
                                {
                                    Session["RoleID"] = r.RoleId;
                                    return RedirectToLocal(ReturnUrl);
                                }
                            }
                            return View();

                        case SignInStatus.Failure:
                            ModelState.AddModelError("", "Invalid Login Attempt");
                            return View(model);

                    }
                }
                else
                {
                    HttpCookie cookie = new HttpCookie("Customer");
                    if (model.RememberMe == true)
                    {

                        cookie["UserName"] = model.Email;
                        //Encrypt Password for cookie
                        byte[] b = ASCIIEncoding.ASCII.GetBytes(model.Password);
                        string encryptedPassword = Convert.ToBase64String(b);

                        cookie["Password"] = encryptedPassword;
                        cookie.Expires = DateTime.Now.AddDays(1);
                        HttpContext.Response.Cookies.Add(cookie);

                    }
                    else
                    {
                        cookie.Expires = DateTime.Now.AddDays(-1);
                        HttpContext.Response.Cookies.Add(cookie);
                    }

                    var customer = _customer.checkUser(model.Email, model.Password);
                    if (customer != null)
                    {
                        Session["Customer"] = customer;
                        Session["UserName"] = customer.CustName;
                        Session["UserID"] = customer.CustId;
                        Session["UserMobile"] = customer.Mobile;

                        
                        using (var redis = new RedisClient(ConfigurationManager.AppSettings["host"].ToString(), 
                            Convert.ToInt32(ConfigurationManager.AppSettings["port"].ToString())))
                        {
                            if(redis.ContainsKey("Customer " + Session["UserID"].ToString()))
                            {
                                var wrapper =redis.As<Customer>();
                                var result = wrapper.GetValue("Customer " + Session["UserID"].ToString());
                                return RedirectToAction("ProductList", "Product");
                            }
                            else
                            {
                                redis.As<Customer>().SetValue("Customer " + Session["UserID"].ToString(), (Customer)Session["Customer"], TimeSpan.FromSeconds(120));
                                return RedirectToAction("ProductList", "Product");
                            }

                        }

                        //return RedirectToAction("ProductList", "Product");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Invalid Login !!");

                    }
                }

            }

            return View();
        }


        [NonAction]
        private ActionResult RedirectToLocal(string returnurl)
        {
            if (returnurl == null)
            {
                return RedirectToAction("Index", "Home");
            }

            if (!String.IsNullOrEmpty(returnurl) && Url.IsLocalUrl(returnurl))
            {
                return Redirect(returnurl);
            }

            return View("Login", "Account");
        }


        [HttpPost]
        public ActionResult Logout()
        {
            if (HttpContext.Session["UserName"] != null)
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
            }
            else
            {
                _signinmanager.AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            }
            return RedirectToAction("Login");


        }


    }
}
